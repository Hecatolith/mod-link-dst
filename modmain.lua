PrefabFiles = {
		-- Character builds:
		"link",
		"link_none",

		-- swords:
		"knight_board",
		"master_sword",
		"royal_board",
		"wood_axe",
		"double_axe",
		"royal_clay",

		"linkerang",

		-- tunics:
		"tunic_green",
		"tunic_red",
		"tunic_blue",

		-- masks:
		"bunnyhood",


		-- flacons:
		"oil_zelda",
		"red_potion",
		"blue_potion",


		-- hearts:
		"heart_container",

		-- food:
		"monstercake"
}

Assets = {
    Asset( "IMAGE", "images/saveslot_portraits/link.tex" ),
    Asset( "ATLAS", "images/saveslot_portraits/link.xml" ),

    Asset( "IMAGE", "images/selectscreen_portraits/link.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/link.xml" ),

    Asset( "IMAGE", "images/selectscreen_portraits/link_silho.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/link_silho.xml" ),

    Asset( "IMAGE", "bigportraits/link.tex" ),
    Asset( "ATLAS", "bigportraits/link.xml" ),

		Asset( "IMAGE", "images/map_icons/link.tex" ),
		Asset( "ATLAS", "images/map_icons/link.xml" ),

		Asset( "IMAGE", "images/avatars/avatar_link.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_link.xml" ),

		Asset( "IMAGE", "images/avatars/avatar_ghost_link.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_link.xml" ),

		Asset( "IMAGE", "images/avatars/self_inspect_link.tex" ),
    Asset( "ATLAS", "images/avatars/self_inspect_link.xml" ),

		Asset( "IMAGE", "images/name_link.tex" ),
    Asset( "ATLAS", "images/name_link.xml" ),

    Asset( "IMAGE", "bigportraits/link_none.tex" ),
    Asset( "ATLAS", "bigportraits/link_none.xml" ),

		Asset("ATLAS", "images/hud/triforcetab.xml" ),
		Asset("IMAGE", "images/hud/triforcetab.tex"),

}

local require = GLOBAL.require
local RECIPETABS = GLOBAL.RECIPETABS
local Recipe = GLOBAL.Recipe
local Ingredient = GLOBAL.Ingredient
local TECH = GLOBAL.TECH
local TUNING = GLOBAL.TUNING
local STRINGS = GLOBAL.STRINGS

-- swords:

GLOBAL.STRINGS.NAMES.MASTER_SWORD = "Master Sword"
GLOBAL.STRINGS.RECIPE_DESC.MASTER_SWORD = "The Legendary sword that seals the darkness."

GLOBAL.STRINGS.NAMES.KNIGHT_BOARD = "Epée de chevalier"
GLOBAL.STRINGS.RECIPE_DESC.KNIGHT_BOARD = "Knights of Hyrule once carried this sword."

GLOBAL.STRINGS.NAMES.ROYAL_BOARD = "Epée royale"
GLOBAL.STRINGS.RECIPE_DESC.ROYAL_BOARD = "The Hyrulean royal family would award this sword to knights who achieved remarkable feats."

GLOBAL.STRINGS.NAMES.ROYAL_CLAY = "Espadon royal"
GLOBAL.STRINGS.RECIPE_DESC.ROYAL_CLAY = "A two-handed sword issued to the Hyrulean royal family's immediate guard detail.."

GLOBAL.STRINGS.NAMES.WOOD_AXE = "Hache de bûcheron"
GLOBAL.STRINGS.RECIPE_DESC.WOOD_AXE = "A woodcutter's tool of choice for felling trees. It can also be used to fight."

GLOBAL.STRINGS.NAMES.DOUBLE_AXE = "Hache de maître"
GLOBAL.STRINGS.RECIPE_DESC.DOUBLE_AXE = "This double-sided axe was designed with fighting in mind."


GLOBAL.STRINGS.NAMES.LINKERANG = "Boomerang"
GLOBAL.STRINGS.RECIPE_DESC.LINKERANG = "Boomerang du héros du vent."


-- tunics:

GLOBAL.STRINGS.NAMES.TUNIC_GREEN = "Tunique verte"
GLOBAL.STRINGS.RECIPE_DESC.TUNIC_GREEN = "L'habit traditionnellement porté par Link."

GLOBAL.STRINGS.NAMES.TUNIC_BLUE = "Tunique bleue"
GLOBAL.STRINGS.RECIPE_DESC.TUNIC_BLUE = "Permet de respirer et nager sous l'eau comme un Zora."

GLOBAL.STRINGS.NAMES.TUNIC_RED = "Tunique rouge"
GLOBAL.STRINGS.RECIPE_DESC.TUNIC_RED = "Offre une protection dans les lieux à forte chaleur."

-- masks:
GLOBAL.STRINGS.NAMES.BUNNYHOOD = "Masque du lapin"
GLOBAL.STRINGS.RECIPE_DESC.BUNNYHOOD = "Permet de courir vite."

-- flacons:

GLOBAL.STRINGS.NAMES.OIL_ZELDA = "Huile"
GLOBAL.STRINGS.RECIPE_DESC.OIL_ZELDA = "N'apparaît que dans Twilight Princess (la CDi n'existe pas)."

GLOBAL.STRINGS.NAMES.RED_POTION = "Potion rouge"
GLOBAL.STRINGS.RECIPE_DESC.RED_POTION = "Remplit les Réceptacles en forme de cœur."

GLOBAL.STRINGS.NAMES.BLUE_POTION = "Potion bleue"
GLOBAL.STRINGS.RECIPE_DESC.BLUE_POTION = "Remplit les cœurs et le pouvoir magique."

-- hearts:

GLOBAL.STRINGS.NAMES.HEART_CONTAINER = "Réceptacle de Coeur"
GLOBAL.STRINGS.RECIPE_DESC.HEART_CONTAINER = "♥"

-- food:

GLOBAL.STRINGS.NAMES.MONSTERCAKE = "Gâteau au monstre"
GLOBAL.STRINGS.RECIPE_DESC.MONSTERCAKE = "Recette royale."

-- New recipetabs:
STRINGS.TABS.TRIFORCE_TAB = "Triforce"GLOBAL.RECIPETABS['TRIFORCE_TAB'] = {str = "TRIFORCE_TAB", sort=990, icon = "triforcetab.tex", icon_atlas = "images/hud/triforcetab.xml"}

-- swords:

local wood_axe_recipe = AddRecipe("wood_axe", -- name
		{ Ingredient("flint", 1), Ingredient("twigs", 1)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.NONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/wood_axe.xml" )  -- atlas

local double_axe_recipe = AddRecipe("double_axe", -- name
		{ Ingredient("flint", 2), Ingredient("twigs", 1)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_ONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/double_axe.xml" )  -- atlas

local master_sword_recipe = AddRecipe("master_sword", -- name
		{ Ingredient("flint", 5), Ingredient("moonrocknugget", 2), Ingredient("bluegem", 1)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_TWO,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/master_sword.xml" )  -- atlas

local knight_board_recipe = AddRecipe("knight_board", -- name
		{ Ingredient("flint", 2), Ingredient("rocks", 2)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.NONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/knight_board.xml" )  -- atlas

local royal_board_recipe = AddRecipe("royal_board", -- name
		{ Ingredient("flint", 2), Ingredient("goldnugget", 2)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_ONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/royal_board.xml" )  -- atlas

local royal_clay_recipe = AddRecipe("royal_clay", -- name
		{ Ingredient("flint", 4), Ingredient("goldnugget", 4)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_ONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/royal_clay.xml" )  -- atlas



local linkerang_recipe = AddRecipe("linkerang", -- name
		{ Ingredient("twigs", 2)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_ONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/linkerang.xml" )  -- atlas

-- tunics:

local tunic_green_recipe = AddRecipe("tunic_green", -- name
		{ Ingredient("cutgrass", 20)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_ONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/tunic_green.xml" )  -- atlas

local tunic_blue_recipe = AddRecipe("tunic_blue", -- name
		{ Ingredient("cutgrass", 20), Ingredient("ice", 5)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_TWO,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/tunic_blue.xml" )  -- atlas

local tunic_red_recipe = AddRecipe("tunic_red", -- name
		{ Ingredient("cutgrass", 20), Ingredient("charcoal", 5)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_TWO,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/tunic_red.xml" )  -- atlas

-- masks:

local tunic_red_recipe = AddRecipe("bunnyhood", -- name
		{ Ingredient("razor", 1), Ingredient("rabbit", 1)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.SCIENCE_ONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/bunnyhood.xml" )  -- atlas

-- flacons:

local oil_zelda_recipe = AddRecipe("oil_zelda", -- name
		{ Ingredient("seeds", 5)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.NONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/oil_zelda.xml" )  -- atlas

local red_potion_recipe = AddRecipe("red_potion", -- name
		{Ingredient("petals", 10), Ingredient("red_cap", 1)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.NONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/red_potion.xml" )  -- atlas

local blue_potion_recipe = AddRecipe("blue_potion", -- name
		{ Ingredient("petals", 10), Ingredient("blue_cap", 1)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.NONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/blue_potion.xml" )  -- atlas

-- hearts:

local heart_container_recipe = AddRecipe("heart_container", -- name
		{ Ingredient(GLOBAL.CHARACTER_INGREDIENT.HEALTH, 100)},
		RECIPETABS.TRIFORCE_TAB,
		TECH.NONE,
		nil,
		nil,
		nil,
		nil,
		"sword", -- builder_tag
		"images/inventoryimages/heart_container.xml" )  -- atlas

-- food:

local monstercake = {
	name = "monstercake",
	test = function(cooker, names, tags) return tags.egg >= 2 and names.butter and tags.monster >= 1 end,
	priority = 1,
	weight = 1,
	foodtype="GENERIC",
	health = TUNING.HEALING_HUGE,
	hunger = TUNING.CALORIES_HUGE,
	sanity = TUNING.SANITY_HUGE,
	perishtime = TUNING.PERISH_SUPERSLOW,
	cooktime = 0.75,
}
AddCookerRecipe("cookpot", monstercake)

-- The character select screen lines
STRINGS.CHARACTER_TITLES.link = "Le héros du crépuscule"
STRINGS.CHARACTER_NAMES.link = "Link"
STRINGS.CHARACTER_DESCRIPTIONS.link = "Beaucoup trop bon."
STRINGS.CHARACTER_QUOTES.link = "\"...\""

local function AddDamageModifier(self, key, mod)
  	self.attack_damage_modifiers[key] = mod
end

local function RemoveDamageModifier(self, key)
  	self.attack_damage_modifiers[key] = nil
end

local function GetDamageModifier(self)
  	local mod = 1
  	for k,v in pairs(self.attack_damage_modifiers) do
    		mod = mod * v
  		end
  	return mod
end

local function RegisterNewCombatMembers(self)
	  self.attack_damage_modifiers = {} -- % modifiers on self:CalcDamage()
	  self.AddDamageModifier = AddDamageModifier
	  self.RemoveDamageModifier = RemoveDamageModifier
	  self.GetDamageModifier = GetDamageModifier
end

AddComponentPostInit("combat", function(self)
		    if self.attack_damage_modifiers == nil then -- check if another mod already added this. cause we should not add this twice
		        RegisterNewCombatMembers(self)

		        local CalcDamageOld = self.CalcDamage

		        self.CalcDamage = function(self, target, weapon, multiplier)
		        		local dmg = CalcDamageOld(self, target, weapon, multiplier)
		        		local bonus = self.damagebonus or 0 --not affected by multipliers

		          	return (dmg-bonus) * self:GetDamageModifier() + bonus
		        end
		    end
		end
)


-- Quick check if host
local IsMasterSim = function()
		if not GLOBAL.TheWorld.ismastersim then
				return false
		end
		return true
end

-- Custom speech strings
STRINGS.CHARACTERS.LINK = require "speech_link"

-- Let the game know character is male, female, or robot
table.insert(GLOBAL.CHARACTER_GENDERS.MALE, "link")

-- The character's name as appears in-game
STRINGS.NAMES.LINK = "Link"

AddMinimapAtlas("images/map_icons/link.xml")

-- Add mod character to mod character list. Also specify a gender. Possible genders are MALE, FEMALE, ROBOT, NEUTRAL, and PLURAL.
AddModCharacter("link")

modimport "get_item_with_linkerang.lua"
