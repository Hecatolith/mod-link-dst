
local MakePlayerCharacter = require "prefabs/player_common"

local assets = {

    Asset("SCRIPT", "scripts/prefabs/player_common.lua"),
}

local prefabs = {}

-- Custom starting items
local start_inv = {
}

-- When the character is revived from human
local function onbecamehuman(inst)
  	-- Set speed when reviving from ghost (optional)
  	inst.components.locomotor:SetExternalSpeedMultiplier(inst, "link_speed_mod", 1)
end

local function onbecameghost(inst)
  	-- Remove speed modifier when becoming a ghost
    inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "link_speed_mod")
end

-- When loading or spawning the character
local function onload(inst)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
end

-- This initializes for both the server and client. Tags can be added here.
local common_postinit = function(inst)
  	-- Minimap icon
  	inst.MiniMapEntity:SetIcon( "link.tex" )
  	inst:AddTag("sword")
  	inst:AddTag("link")
end

-- This initializes for the server only. Components are added here.
local master_postinit = function(inst)
  	-- Stats
  	inst.components.health:SetMaxHealth(200)
  	inst.components.hunger:SetMax(150)
  	inst.components.sanity:SetMax(200)

  	inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1)
  	inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1)

  	inst.components.health:SetAbsorptionAmount(.5)
  	inst.components.moisture:SetInherentWaterproofness(.4)


  	-- Hunger rate (optional)
  	inst.components.hunger.hungerrate = .8 * TUNING.WILSON_HUNGER_RATE

  	inst.OnLoad = onload
      inst.OnNewSpawn = onload

  	-- choose which sounds this character will play
  	inst.soundsname = "wilson"

  	-- todo: Add an example special power here.

end

return MakePlayerCharacter("link", prefabs, assets, common_postinit, master_postinit, start_inv)
