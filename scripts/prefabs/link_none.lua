local assets =
{
	Asset( "ANIM", "anim/link.zip" ),
	Asset( "ANIM", "anim/ghost_link_build.zip" ),
}

local skins =
{
	normal_skin = "link",
	ghost_skin = "ghost_link_build",
}

local base_prefab = "link"

local tags = {"LINK", "CHARACTER"}

return CreatePrefabSkin("link_none",
{
	base_prefab = base_prefab, 
	skins = skins, 
	assets = assets,
	tags = tags,
	
	skip_item_gen = true,
	skip_giftable_gen = true,
})