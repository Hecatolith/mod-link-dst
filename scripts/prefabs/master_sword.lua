local assets =
{
	-- Animation files used for the item.
	Asset("ANIM", "anim/mastersword.zip"),
	Asset("ANIM", "anim/swap_mastersword.zip"),

	-- Inventory image and atlas file used for the item.
    Asset("ATLAS", "images/inventoryimages/master_sword.xml"),
    Asset("IMAGE", "images/inventoryimages/master_sword.tex"),
}

local prefabs =
{
}

local function OnEquip(inst, owner)
	owner.AnimState:OverrideSymbol("swap_object", "swap_mastersword", "mastersword")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function OnUnequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function fn()

    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
	inst.entity:AddMiniMapEntity()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("mastersword")
    inst.AnimState:SetBuild("mastersword")
    inst.AnimState:PlayAnimation("idle")

	inst:AddTag("sharp")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.entity:SetPristine()

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(120)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "master_sword"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/master_sword.xml"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip( OnEquip )
    inst.components.equippable:SetOnUnequip( OnUnequip )

	inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(420)
    inst.components.finiteuses:SetUses(420)
    inst.components.finiteuses:SetOnFinished(inst.Remove)

	MakeHauntableLaunch(inst)

    return inst
end

return Prefab("common/inventory/master_sword", fn, assets, prefabs)
