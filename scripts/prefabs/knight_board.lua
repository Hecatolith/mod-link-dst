local assets =
{
	-- Animation files used for the item.
		Asset("ANIM", "anim/knightboard.zip"),
		Asset("ANIM", "anim/swap_knightboard.zip"),

	-- Inventory image and atlas file used for the item.
    Asset("ATLAS", "images/inventoryimages/knight_board.xml"),
    Asset("IMAGE", "images/inventoryimages/knight_board.tex"),
}

local prefabs =
{
}

local function OnEquip(inst, owner)
		owner.AnimState:OverrideSymbol("swap_object", "swap_knightboard", "knightboard")
		owner.AnimState:Show("ARM_carry")
		owner.AnimState:Hide("ARM_normal")
end

local function OnUnequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function fn()

    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
		inst.entity:AddMiniMapEntity()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("knightboard")
    inst.AnimState:SetBuild("knightboard")
    inst.AnimState:PlayAnimation("idle")

		inst:AddTag("sharp")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.entity:SetPristine()

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(40)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "knight_board"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/knight_board.xml"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip( OnEquip )
    inst.components.equippable:SetOnUnequip( OnUnequip )

		inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(60)
    inst.components.finiteuses:SetUses(60)
    inst.components.finiteuses:SetOnFinished(inst.Remove)

		MakeHauntableLaunch(inst)

    return inst
end

return Prefab("common/inventory/knight_board", fn, assets, prefabs)
