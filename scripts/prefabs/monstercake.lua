local Assets =
{
  Asset("ANIM", "anim/monstercake.zip"),
  Asset("ATLAS", "images/inventoryimages/monstercake.xml"),
}

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	MakeInventoryPhysics(inst)
	MakeSmallBurnable(inst)
	MakeSmallPropagator(inst)

	inst.AnimState:SetBank("monstercake")
	inst.AnimState:SetBuild("monstercake")
	inst.AnimState:PlayAnimation("idle", false)

	inst:AddTag("preparedfood")

	inst:AddComponent("edible")
	inst.components.edible.healthvalue = TUNING.HEALING_HUGE
	inst.components.edible.hungervalue = TUNING.CALORIES_HUGE
	inst.components.edible.foodtype = "GENERIC"
	inst.components.edible.sanityvalue = TUNING.SANITY_HUGE

	inst:AddComponent("inspectable")

	inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.atlasname = "images/inventoryimages/monstercake.xml"
	inst.components.inventoryitem.nobounce = true

	inst:AddComponent("perishable")
	inst.components.perishable:SetPerishTime(TUNING.PERISH_SUPERSLOW)
	inst.components.perishable:StartPerishing()
	inst.components.perishable.onperishreplacement = "spoiled_food"

	inst:AddComponent("bait")

	inst:AddComponent("tradable")

	return inst
end

return Prefab( "common/inventory/monstercake", fn, Assets )
