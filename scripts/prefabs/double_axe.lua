local assets =
{
	-- Animation files used for the item.
	Asset("ANIM", "anim/doubleaxe.zip"),
	Asset("ANIM", "anim/swap_doubleaxe.zip"),

	-- Inventory image and atlas file used for the item.
    Asset("ATLAS", "images/inventoryimages/double_axe.xml"),
    Asset("IMAGE", "images/inventoryimages/double_axe.tex"),
}

local prefabs =
{
}

local function OnEquip(inst, owner)
	owner.AnimState:OverrideSymbol("swap_object", "swap_doubleaxe", "doubleaxe")
    owner.AnimState:Show("ARM_carry")
    owner.AnimState:Hide("ARM_normal")
end

local function OnUnequip(inst, owner)
    owner.AnimState:Hide("ARM_carry")
    owner.AnimState:Show("ARM_normal")
end

local function fn()

    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
	inst.entity:AddMiniMapEntity()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("doubleaxe")
    inst.AnimState:SetBuild("doubleaxe")
    inst.AnimState:PlayAnimation("idle")

	inst:AddTag("sharp")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.entity:SetPristine()

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(34)

	inst:AddComponent("tool")
    inst.components.tool:SetAction(ACTIONS.CHOP, 10)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "double_axe"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/double_axe.xml"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip( OnEquip )
    inst.components.equippable:SetOnUnequip( OnUnequip )

	inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(250)
    inst.components.finiteuses:SetUses(250)
    inst.components.finiteuses:SetOnFinished(inst.Remove)
	inst.components.finiteuses:SetConsumption(ACTIONS.CHOP, 1)

	MakeHauntableLaunch(inst)

    return inst
end

return  Prefab("common/inventory/double_axe", fn, assets, prefabs)
