local assets =
{
	Asset("ANIM", "anim/red_potion.zip"),
  Asset("ATLAS", "images/inventoryimages/red_potion.xml"),
  Asset("IMAGE", "images/inventoryimages/red_potion.tex"),
}

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("red_potion")
    inst.AnimState:SetBuild("red_potion")
    inst.AnimState:PlayAnimation("idle")

    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "red_potion"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/red_potion.xml"

    inst:AddComponent("healer")
    inst.components.healer:SetHealthAmount(TUNING.HEALING_LARGE)

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("common/inventory/red_potion", fn, assets, prefabs)
