local assets =
{
	Asset("ANIM", "anim/blue_potion.zip"),
  Asset("ATLAS", "images/inventoryimages/blue_potion.xml"),
  Asset("IMAGE", "images/inventoryimages/blue_potion.tex"),
}

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("blue_potion")
    inst.AnimState:SetBuild("blue_potion")
    inst.AnimState:PlayAnimation("idle")

    MakeInventoryFloatable(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "blue_potion"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/blue_potion.xml"

    inst:AddComponent("healer")
    inst.components.healer:SetHealthAmount(TUNING.HEALING_SUPERHUGE)

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("common/inventory/blue_potion", fn, assets, prefabs)
