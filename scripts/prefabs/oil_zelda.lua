local assets=
{
    Asset("ANIM", "anim/oil_zelda.zip"),
    Asset("IMAGE", "images/inventoryimages/oil_zelda.tex"),
    Asset("ATLAS", "images/inventoryimages/oil_zelda.xml"),
}

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("oil_zelda")
    inst.AnimState:SetBuild("oil_zelda")
    inst.AnimState:PlayAnimation("idle")

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "oil_zelda"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/oil_zelda.xml"

    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = TUNING.LARGE_FUEL

    MakeSmallBurnable(inst, TUNING.SMALL_BURNTIME)
    MakeSmallPropagator(inst)

    MakeHauntableLaunchAndIgnite(inst)


    return inst
end


return Prefab( "common/inventory/oil_zelda", fn, assets, prefabs )
