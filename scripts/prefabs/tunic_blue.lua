local assets=
{
	Asset("ANIM", "anim/tunic_blue.zip"),
  Asset("ATLAS", "images/inventoryimages/tunic_blue.xml"),
  Asset("IMAGE", "images/inventoryimages/tunic_blue.tex"),
}


local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_body", "tunic_blue", "swap_body")
    owner.components.temperature.mintemp = 15
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_body")
    owner.components.temperature.mintemp = -20
end

local function fn(Sim)
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    MakeInventoryPhysics(inst)

    if not TheWorld.ismastersim then
        return inst
    end

		inst:AddComponent("armor")
    inst.components.armor:InitCondition(420, TUNING.FULL_ABSORPTION)

    inst.AnimState:SetBank("tunic_blue")
    inst.AnimState:SetBuild("tunic_blue")
    inst.AnimState:PlayAnimation("anim")

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
  	 inst.components.inventoryitem.atlasname = "images/inventoryimages/tunic_blue.xml"

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BODY

    inst.components.equippable:SetOnEquip( onequip )
    inst.components.equippable:SetOnUnequip( onunequip )

   	inst:AddComponent("insulator")
    inst.components.insulator.insulation = math.huge

    return inst
end


return Prefab( "common/inventory/tunic_blue", fn, assets, prefabs )
