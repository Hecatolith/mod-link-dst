local assets=
{
  Asset("ANIM", "anim/tunic_green.zip"),
  Asset("ATLAS", "images/inventoryimages/tunic_green.xml"),
  Asset("IMAGE", "images/inventoryimages/tunic_green.tex"),
}


local function onequip(inst, owner)
  owner.AnimState:OverrideSymbol("swap_body", "tunic_green", "swap_body")
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_body")
end

local function fn(Sim)
  local inst = CreateEntity()

  inst.entity:AddTransform()
  inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.AnimState:SetBank("tunic_green")
    inst.AnimState:SetBuild("tunic_green")
    inst.AnimState:PlayAnimation("anim")

    inst:AddComponent("armor")
    inst.components.armor:InitCondition(2000, TUNING.FULL_ABSORPTION)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = "images/inventoryimages/tunic_green.xml"

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BODY

    inst.components.equippable:SetOnEquip( onequip )
    inst.components.equippable:SetOnUnequip( onunequip )

    return inst
end


return Prefab( "common/inventory/tunic_green", fn, assets, prefabs )
