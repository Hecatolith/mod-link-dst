local assets=
{
	Asset("ANIM", "anim/tunic_red.zip"),
  Asset("ATLAS", "images/inventoryimages/tunic_red.xml"),
  Asset("IMAGE", "images/inventoryimages/tunic_red.tex"),
}


local function GetHeatFn(inst)
	return -200
end

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("swap_body", "tunic_red", "swap_body")
    owner.components.temperature.maxtemp = 60
		owner.components.health.fire_damage_scale = 0
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("swap_body")
		owner.components.temperature.maxtemp = 95
		owner.components.health.fire_damage_scale = 1
end

local function fn(Sim)
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
    MakeInventoryPhysics(inst)


		inst:AddComponent("armor")
    inst.components.armor:InitCondition(420, TUNING.FULL_ABSORPTION)

    inst.AnimState:SetBank("tunic_red")
    inst.AnimState:SetBuild("tunic_red")
    inst.AnimState:PlayAnimation("anim")

    inst:AddComponent("inspectable")

	if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inventoryitem")
  	 inst.components.inventoryitem.atlasname = "images/inventoryimages/tunic_red.xml"

    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BODY

    inst.components.equippable:SetOnEquip( onequip )
    inst.components.equippable:SetOnUnequip( onunequip )

    return inst
end


return Prefab( "common/inventory/tunic_red", fn, assets, prefabs )
